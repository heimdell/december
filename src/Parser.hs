
module Parser where

import Control.Applicative

import Data.List

import Parsing
import Program
import Lexeme

program :: P Lexeme Program
program = call

call :: P Lexeme Program
call = do
  x <- qterm
  xs <- many ((,) <$> getPos <*> qterm)
  return $ foldl (\f (p, x) -> App p f x) x xs

qterm :: P Lexeme Program
qterm = do
  p <- getPos
  t <- term
  q <- many do
    match DOT
    name `aka` "field name"
  return case q of
    [] -> t
    q  -> Qual p t q

term :: P Lexeme Program
term =
    tuple
    <|> list
    <|> object
    <|> let_
    <|> match_
    <|> lam
    <|> var
    <|> Lit <$> getPos <*> lit

var :: P Lexeme Program
var = do
  p <- getPos
  n <- name
  return $ Var p n

lit :: P Lexeme Literal
lit = string <|> integer <|> float

string :: P Lexeme Literal
string = prim (Expected' "string literal") \case
  (_, STRING s) : rest -> Just (S $ intercalate ['"'] s, rest)
  _                    -> Nothing

integer :: P Lexeme Literal
integer = prim (Expected' "integer literal") \case
  (_, NUMBER s) : rest -> case reads s of
    [(i, _)] -> Just (I i, rest)
    _        -> Nothing
  _ -> Nothing

float :: P Lexeme Literal
float = prim (Expected' "floating point literal") \case
  (_, NUMBER s) : rest -> case reads s of
    [(i, _)] -> Just (F i, rest)
    _        -> Nothing
  _ -> Nothing

lam :: P Lexeme Program
lam = do
  match LAMBDA `aka` "function"
  args <- many ((,) <$> getPos <*> patTerm)
  match ARROW
  b <- program
  return $ foldr (uncurry Lam) b args

tuple :: P Lexeme Program
tuple = do
  p <- getPos
  match LPAREN `aka` "tuple or group start"
  elems <- ((,) <$> getPos <*> program) `sepBy` (match SEP `aka` "tuple sep")
  match RPAREN `aka` "tuple or group start"
  return case elems of
    [] -> Variant p "Unit" 0
    [(_, a)] -> a
    els ->
      let len = length els
          ctor = Variant p ("Tuple" ++ show len) len
      in foldl (\f (p, x) -> App p f x) ctor els

list :: P Lexeme Program
list = do
  p <- getPos
  match LBRACKET `aka` "list"
  match LBRACE
  elems <- program `sepBy` (match SEP `aka` "list separator")
  do try do
        match RBRACE   `aka` "list end?"
        optional $ match SEP
        match RBRACKET `aka` "list end"
    <|> do
        match RBRACKET `aka` "list end"
        match RBRACE   `aka` "list end#2"
  return
    let cons = Variant p ("Cons") 2
        nil  = Variant p ("Nil") 0
    in foldr (\x xs -> App p (App p cons x) xs) nil elems

object :: P Lexeme Program
object = do
  p <- getPos
  match LBRACE `aka` "object"
  match LBRACE
  decls <- decl' `sepBy` match SEP
  match RBRACE `aka` "closing object?"
  optional $ match SEP
  match RBRACE `aka` "closing object"
  return $ Create p decls

name :: P Lexeme String
name = project (Expected' "name") \case
  NAME n -> Just n
  _      -> Nothing

varName :: P Lexeme String
varName = project (Expected' "name") \case
  NAME n | head n `elem` (['a'.. 'z'] ++ "_") -> Just n
  _      -> Nothing

ctor :: P Lexeme String
ctor = project (Expected' "constructor") \case
  NAME n | head n `elem` ['A'.. 'Z'] -> Just n
  _      -> Nothing

let_ :: P Lexeme Program
let_ = do
  pos <- getPos
  match LET `aka` "let-expression"
  match LBRACE
  decls <- decl `sepBy1` (match SEP `aka` "declaration sep")
  match RBRACE
  p <- program
  return $ Let pos decls p

decl :: P Lexeme Decl
decl = variant <|> fundecl

decl' :: P Lexeme Decl
decl' = variant <|> fundecl'

variant :: P Lexeme Decl
variant = do
  p <- getPos
  match VARIANT `aka` "variant"
  n     <- ctor
  args  <- many varName
  return $ Fun p n (Variant p n (length args))

int :: P Lexeme Int
int = project (Expected' "int") \case
  NUMBER sn -> case reads sn of
    [(n, _)] -> Just n
    _        -> Nothing
  _ -> Nothing

fundecl :: P Lexeme Decl
fundecl = do
  pos <- getPos
  n    <- varName `aka` "function name"
  args <- many ((,) <$> getPos <*> patTerm)
  match EQUALS `aka` "start of function body"
  p    <- program
  return $ Fun pos n (foldr (uncurry Lam) p args)

fundecl' :: P Lexeme Decl
fundecl' = do
  pos <- getPos
  n <- varName `aka` "function name"
  do try do
      args <- many ((,) <$> getPos <*> patTerm)
      match EQUALS `aka` "start of function body"
      p    <- program
      return $ Fun pos n (foldr (uncurry Lam) p args)
   <|> do
     return $ Capture pos n

match_ :: P Lexeme Program
match_ = do
  p <- getPos
  match CASE `aka` "pattern-match"
  subj <- program
  match OF
  match LBRACE
  alts <- alt `sepBy` (match SEP `aka` "alternative sep")
  match RBRACE
  return $ Match p subj alts

alt :: P Lexeme (Pattern, Program)
alt = do
  match BAR `aka` "alternative"
  p <- pat
  match ARROW `aka` "alternative body"
  b <- program
  return (p, b)

pat :: P Lexeme Pattern
pat = isVariant <|> patTerm

patTerm :: P Lexeme Pattern
patTerm =
  isTuple
  <|> isList
  <|> isVar
  <|> isObj
  <|> strict
  <|> wildcard
  <|> isLit

isLit :: P Lexeme Pattern
isLit = do
  p <- getPos
  l <- lit
  return $ IsLit p l

isTuple :: P Lexeme Pattern
isTuple = do
  pos <- getPos
  match LPAREN `aka` "tuple pattern"
  elems <- pat `sepBy` match SEP
  match RPAREN `aka` "tuple pattern end"
  return case elems of
    [] -> IsCtor pos "Unit" []
    [a] -> a
    els ->
      let len = length els
          ctor = "Tuple" ++ show len
      in IsCtor pos ctor elems

isList :: P Lexeme Pattern
isList = do
  p <- getPos
  match LBRACKET `aka` "list"
  match LBRACE
  elems <- ((,) <$> getPos <*> pat) `sepBy` (match SEP `aka` "list separator")
  do try do
        match RBRACE   `aka` "list end?"
        optional $ match SEP
        match RBRACKET `aka` "list end"
    <|> do
        match RBRACKET `aka` "list end"
        match RBRACE   `aka` "list end#2"
  return
    let cons (p, x) xs = IsCtor p "Cons" [x, xs]
        nil            = IsCtor p "Nil" []
    in  foldr cons nil elems

isVar :: P Lexeme Pattern
isVar = do
  p <- getPos
  n <- varName `aka` "binding name"
  return (IsVar p n)

isObj :: P Lexeme Pattern
isObj = do
  p <- getPos
  match LBRACE `aka` "object pattern"
  match LBRACE
  isDecls <- isDecl `sepBy` match SEP
  match RBRACE
  optional $ match SEP
  match RBRACE
  return $ IsObj p isDecls

isDecl :: P Lexeme (Name, Maybe Pattern)
isDecl = do
  n <- name `aka` "declaration name"
  mbPat <- optional do
    match EQUALS `aka` "declaration pattern"
    pat
  return (n, mbPat)

isVariant :: P Lexeme Pattern
isVariant = do
  p    <- getPos
  c    <- ctor `aka` "variant pattern"
  args <- many patTerm
  return $ IsCtor p c args

strict :: P Lexeme Pattern
strict = do
  pos <- getPos
  match BANG `aka` "strict pattern"
  p <- patTerm
  return $ Strict pos p

wildcard :: P Lexeme Pattern
wildcard = do
  p <- getPos
  match WILDCARD `aka` "wildcard pattern"
  return $ Any p

sepBy :: Alternative f => f a -> f s -> f [a]
sepBy p s = liftA2 (:) p ((s *> sepBy1 p s) <|> pure []) <|> pure []

sepBy1 :: Alternative f => f a -> f s -> f [a]
sepBy1 p s = scan
  where
    scan = liftA2 (:) p ((s *> scan) <|> pure [])

infixr 4 <&>
(<&>) :: Functor f => f a -> (a -> b) -> f b
(<&>) = flip fmap
