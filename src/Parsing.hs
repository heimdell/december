
module Parsing
  ( module Parsing
  , module ParsingBase
  , module Pos
  )
  where

import Control.Applicative
import Control.Monad

import Data.List

import Pos
import ParsingBase

type Stream l = [(Pos, l)]

data Error l
  = Expected l
  | Expected' String
  | Tree String [Error l]

instance Show l => Show (Error l) where
  show = \case
    Expected l -> show l
    Expected' s -> s
    Tree s errs -> s ++ " (" ++ intercalate " or " (map show errs) ++ ")"

type P l = Parser (Error l) (Stream l)

getPos :: P l Pos
getPos = do
  prim (Expected' "program") \case
    rest@((pos, _) : _) -> Just (pos, rest)
    _                   -> Nothing

match :: Eq l => l -> P l ()
match lexeme = do
  prim (Expected lexeme) \case
    (pos, lex) : rest
      | lex == lexeme -> Just (lex, rest)
    _ -> Nothing
  return ()

project :: Error l -> (l -> Maybe a) -> P l a
project e proj = do
  prim e \case
    (pos, lex) : rest -> do
      a <- proj lex
      return (a, rest)

    _ -> Nothing

aka :: P l a -> String -> P l a
aka (Parser p) name = Parser $ \s ->
  case p s of
    (Left errs, str) -> (Left [Tree name errs], str)
    other            -> other