
module Program where

import Data.String

import GHC.OverloadedLabels
import GHC.TypeLits
import GHC.Exts

import Text.PrettyPrint hiding ((<>))

import Pos

data Program
  = Var     Pos Name
  | Qual    Pos Program  [Name]
  | App     Pos Program  Program
  | Let     Pos [Decl]   Program
  | Lam     Pos Pattern  Program
  | Match   Pos Program  Alts
  | Create  Pos [Decl]
  | Lit     Pos Literal
  | Variant Pos Name     Int

data Decl
  = Fun     Pos Name    Program
  | Capture Pos Name

type Alts  = [(Pattern, Program)]
type Name  = String

data Literal
  = S String
  | I Integer
  | F Double
  deriving stock (Eq)

data Pattern
  = IsVar  Pos Name
  | IsObj  Pos [(Name, Maybe Pattern)]
  | IsCtor Pos Name [Pattern]
  | IsLit  Pos Literal
  | Strict Pos Pattern
  | Any    Pos

ppProg :: Program -> Doc
ppProg p = case p of
  Var     _ name       -> text name
  Qual    _ src path   -> cat $ punctuate "." $ ppProg src : map text path
  App     _ f xs       -> parens (ppProg f <+> ppProg xs)
  Let     _ decls body -> indent "let" (vcat (map ppDecl decls))
                     $$ ppProg body
  Lam     _ n  b       -> parens $ indent (("\\" <> ppPat n) <+> "->")
                               (ppProg b)
  Match   _ subj alts  -> indent ("case" <+> ppProg subj <+> "of")
                               (vcat (map ppAlt alts))
  Create  _ names      -> braces (fsep (map ppDecl names))
  Lit     _ lit        -> ppLit lit
  Variant _ name arity -> text name <> "/" <> int arity

ppLit :: Literal -> Doc
ppLit l = case l of
  S str -> quotes (text str)
  I i   -> integer i
  F dbl -> double  dbl

ppPat :: Pattern -> Doc
ppPat pat = case pat of
  IsVar  _ name      -> text name
  IsObj  _ isDecls   -> braces (fsep (map ppIsDecl isDecls))
  IsCtor _ name pats -> parens (text name <+> fsep (map ppPat pats))
  IsLit  _ lit       -> ppLit lit
  Strict _ sub       -> "!" <> parens (ppPat sub)
  Any    _           -> "_"

ppIsDecl :: (Name, Maybe Pattern) -> Doc
ppIsDecl (name, mpat) = case mpat of
  Just pat -> (text name <+> "=" <+> ppPat pat) <> semi
  Nothing  -> text name <> semi

ppAlt :: (Pattern, Program) -> Doc
ppAlt (pat, prog) =
  indent ("|" <+> ppPat pat <+> "->")
         (ppProg prog)

ppDecl :: Decl -> Doc
ppDecl = \case
  Fun _ n (collectArgs -> (az, p)) ->
    indent (text n <+> fsep (map ppPat az) <+> "=") (ppProg p)
  Capture _ n -> text n

collectArgs :: Program -> ([Pattern], Program)
collectArgs p = case p of
  Lam _ n (collectArgs -> (rest, body)) -> (n : rest, body)
  other                               -> ([], other)

indent :: Doc -> Doc -> Doc
indent a b = hang a 2 b
