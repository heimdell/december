
module Reconstruct where

import Control.Monad.State
import Control.Monad.Writer

import Pos

-- | Build printable colored representation of AST.
reconstruct :: forall a. (Show a) => [(Pos, a)] -> String
reconstruct
  = execWriter
  . flip evalStateT (1, 1)
  . mapM_ findPlace
  where
    findPlace :: (Pos, a) -> StateT (Int, Int) (Writer String) ()
    findPlace (pos, a) = do
      (l, c) <- get
      tell (replicate (line pos - l) '\n')

      tell (replicate (if line pos - l > 0 then col pos else col pos - c) ' ')
      tell (show a)

      let end = foldl (flip step) pos (show a)
      put (line end, col end)
