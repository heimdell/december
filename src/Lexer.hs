
module Lexer where

import Control.Applicative

import Data.Maybe

import Lexing
import Lexeme
import Indentation

decodePunctuation :: Char -> Lexeme
decodePunctuation c = fromJust $ lookup c
  [ ('(', LPAREN)
  , (')', RPAREN)
  , ('{', LBRACE)
  , ('}', RBRACE)
  , ('[', LBRACKET)
  , (']', RBRACKET)
  , (';', SEP)
  , (',', SEP)
  ]

decodeName :: String -> Lexeme
decodeName n = case n of
  "let"     -> LET
  "case"    -> CASE
  "of"      -> OF
  "variant" -> VARIANT
  "_"       -> WILDCARD
  "from"    -> FROM
  "use"     -> USE
  _         -> NAME n

decodeOp :: String -> Lexeme
decodeOp op = case op of
  "="  -> EQUALS
  "|"  -> BAR
  "->" -> ARROW
  "\\" -> LAMBDA
  "!"  -> BANG
  "."  -> DOT
  _    -> OP op

lexer :: Parser () Stream [(Pos, Lexeme)]
lexer = do
  spaces
  res <- many $ try do
    ps <- pos
    p  <- com <|> punct <|> name <|> op <|> num <|> str
    spaces
    return (ps, p)
  eos
  return res
  where
    punct = decodePunctuation <$> punctuation "[]{}(),;"
    name  = decodeName        <$> ((:) <$> punctuation alpha <*> lexemeEnd alphanum)
    op    = decodeOp          <$> lexeme opChars
    num   = NUMBER            <$> ((:) <$> punctuation digit <*> lexemeEnd numberBody)
    str   = STRING            <$> some (quoted '"')
    com   = COMMENT           <$> (comments "(*" "*)" <|> comments "//" "\n")

    alpha      = ['a'.. 'z'] ++ ['A'.. 'Z'] ++ "_"
    alphanum   = alpha ++ digit ++ "'"
    opChars    = "!@#$%^&*-+\\/?><.~%^=|:"
    digit      = ['0'.. '9']
    numberBody = digit ++ ".e-"

removeComments :: [(Pos, Lexeme)] -> [(Pos, Lexeme)]
removeComments = filter (not . isComment . snd)
  where
    isComment COMMENT {} = True
    isComment _          = False

generateBracesFromIndent :: [(Pos, Lexeme)] -> [(Pos, Lexeme)]
generateBracesFromIndent = insertBraces initiates ignored (LBRACE, SEP, RBRACE)
  where
    ignored COMMENT {} = True
    ignored _          = False

    initiates LET      = True
    initiates OF       = True
    initiates LBRACE   = True
    initiates LBRACKET = True
    initiates _        = False