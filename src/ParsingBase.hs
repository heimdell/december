
module ParsingBase where

import Control.Applicative
import Control.Monad

import Pos

newtype Parser e s a = Parser
  { runParser :: s -> (Either [e] a, s)
  }

instance Functor (Parser e s) where
  fmap = liftM

instance Applicative (Parser e s) where
  pure a = Parser \s -> (Right a, s)
  (<*>) = ap

instance Monad (Parser e s) where
  Parser pa >>= amb = Parser \s ->
    case pa s of
      (Left  e, s') -> (Left e, s')
      (Right a, s') -> amb a `runParser` s'

instance Eq s => Alternative (Parser e s) where
  empty = Parser \s -> (Left [], s)

  Parser l <|> Parser r = Parser \s ->
    case l s of
      (Left les, s')
        | s == s' ->
          case r s of
            (Left res, s'')
              | s == s''   -> (Left (les ++ res), s)
              | otherwise  -> (Left  res, s'')
            other          -> other
        | otherwise -> (Left  les, s')
      other         -> other

try :: Parser e s a -> Parser e s a
try (Parser p) = Parser \s ->
  case p s of
    (Left es, _) -> (Left es, s)
    other        -> other

die :: e -> Parser e s a
die e = Parser \s -> (Left [e], s)

prim :: e -> (s -> Maybe (a, s)) -> Parser e s a
prim e p = Parser \s ->
  case p s of
    Just (a, s') -> (Right a, s')
    Nothing      -> (Left [e], s)
