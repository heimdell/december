module Indentation where

import Control.Monad.State
import Control.Monad.Writer

import Pos

-- | Put additional "{;}" based on the layout.
insertBraces
  :: forall a
  .  (Eq a, Show a)
  => (a -> Bool)  -- if token initiates layout level
  -> (a -> Bool)  -- if token is ignored
  -> (a, a, a)    -- "{" ";" "}"
  -> [(Pos, a)]
  -> [(Pos, a)]
insertBraces
    initiator
    ignored
    (open, sep, close)
    list
  =
      execWriter
    $ evalStateT (mapM click list *> dropEnoughStack (Pos 1 1) 0)
    $ ([-2], False)
  where
    click :: (Pos, a) -> StateT ([Int], Bool) (Writer [(Pos, a)]) ()
    click (pos, a) = do
      unless (ignored a) do
        dropEnoughStack pos (col pos)

        get >>= \case
          (top : stack, next) -> do

            when next do
              tell [(pos, open)]
              put (col pos : top : stack, False)

            when (col pos == top) do
              tell [(pos, sep)]

            when (initiator a) do
              (s, _) <- get
              put (s, True)

      tell [(pos, a)]

    dropEnoughStack :: Pos -> Int -> StateT ([Int], Bool) (Writer [(Pos, a)]) ()
    dropEnoughStack pos level = do
      get >>= \case
        (top : stack, bool) -> do
          when (top > level && not bool) do
            tell [(pos, close)]
            put (stack, bool)
            dropEnoughStack pos level
