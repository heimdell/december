
module Lexeme where

import Data.List (intercalate)

data Lexeme
  = LPAREN
  | RPAREN
  | LBRACE
  | RBRACE
  | LBRACKET
  | RBRACKET
  | SEP
  | LET
  | EQUALS
  | BAR
  | ARROW
  | CASE
  | OF
  | LAMBDA
  | BANG
  | NUMBER String
  | STRING [String]
  | NAME   String
  | OP     String
  | VARIANT
  | DOT
  | SPACE
  | WILDCARD
  | COMMENT String
  | FROM
  | USE
  deriving stock (Eq)

instance Show Lexeme where
  show = \case
    LPAREN -> "("
    RPAREN -> ")"
    LBRACE -> "{"
    RBRACE -> "}"
    LBRACKET -> "["
    RBRACKET -> "]"
    SEP -> ","
    LET -> "let"
    EQUALS -> "="
    BAR -> "|"
    ARROW -> "->"
    CASE -> "case"
    OF -> "of"
    LAMBDA -> "\\"
    BANG -> "!"
    NUMBER n -> n
    STRING ss -> intercalate "\"" ss
    NAME n -> n
    OP n -> n
    VARIANT -> "variant"
    DOT -> "."
    SPACE -> " "
    WILDCARD -> "_"
    COMMENT s -> "(*" ++ s ++ " *)"
    FROM -> "from"
    USE -> "use"