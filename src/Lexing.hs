
module Lexing
  ( module Lexing
  , module ParsingBase
  , module Pos
  )
  where

import Control.Applicative
import Control.Monad

import Pos
import ParsingBase

type Stream = (Pos, String)

punctuation :: String -> Parser () Stream Char
punctuation = satisfy . flip elem

satisfy :: (Char -> Bool) -> Parser () Stream Char
satisfy p = try do
  c <- prim () \case
    (pos, a : xs) -> Just (a, (step a pos, xs))
    _             -> Nothing
  guard (p c)
  return c

char :: Char -> Parser () Stream Char
char = satisfy . (==)

quoted :: Char -> Parser () Stream String
quoted q = do
  _ <- char q
  block <- many (satisfy (/= q))
  _ <- char q
  return block

string :: String -> Parser () Stream String
string = try . mapM char

comments :: String -> String -> Parser () Stream String
comments open close@(c : _) = do
  _ <- string open
  block <- many (satisfy (/= c))
  _ <- string close
  return block

lexeme :: String -> Parser () Stream String
lexeme = some . punctuation

lexemeEnd :: String -> Parser () Stream String
lexemeEnd = many . punctuation

eos :: Parser () Stream ()
eos = prim () \case
  (p, []) -> Just ((), (p, []))
  _       -> Nothing

spaces :: Parser () Stream ()
spaces = void $ many (punctuation " \n\r\t")

pos :: Parser () Stream Pos
pos = prim () \(p, s) -> Just (p, (p, s))