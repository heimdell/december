
module Pos where

data Pos = Pos
  { col, line :: Int
  }
  deriving (Eq, Ord)

instance Show Pos where
  show (Pos c l) = show l ++ ":" ++ show c

step :: Char -> Pos -> Pos
step '\n' (Pos _ l) = Pos 1 (l + 1)
step  _   (Pos c l) = Pos (c + 1) l

