module Eval where

import Control.Applicative
import Control.Exception
import Control.Monad

import Data.IORef
import Data.List (intercalate)
import Data.Traversable

import System.IO.Unsafe

import Program
import Pos

data Value
  = Ctor     Name Int [Ref]
  | Object   Env
  | Constant Literal
  | Closure  Env Pattern Program
  | Builtin  (Ref -> IO Value)

data Node
  = Done    Value
  | Delayed Env Program

data LitKind = IsS | IsI | IsF
  deriving stock (Eq, Show)

type Env = [(Name, Ref)]
type Ref = IORef Node

instance Show Value where
  show = showVal []

showVal :: [Ref] -> Value -> String
showVal visited = \case
  Ctor n a args
     | a > 0 -> "(\\[" ++ show a ++ "] -> " ++ show n ++ " "
             ++ unwords (map (showRef visited) args) ++ ")"
     | True  -> "(" ++ show n ++ " "
             ++ unwords (map (showRef visited) args) ++ ")"
  Object env -> "{" ++ intercalate "; " (map (showDecl visited) env) ++ "}"
  Constant l -> show (ppLit l)
  Closure _ n p -> "(\\" ++ show (ppPat n) ++ " -> " ++ show (ppProg p) ++ ")"
  Builtin _ -> "<fun>"

showRef :: [Ref] -> Ref -> String
showRef visited ref
  | ref `elem` visited = ".."
  | otherwise = unsafePerformIO do
    mval <- readIORef ref
    return case mval of
      Done v -> showVal (ref : visited) v
      _ -> "??"

showDecl :: [Ref] -> (Name, Ref) -> String
showDecl visited (name, ref) =
  name ++ " = " ++ showRef visited ref


litKind :: Literal -> LitKind
litKind I {} = IsI
litKind S {} = IsS
litKind F {} = IsF

force :: Ref -> IO Value
force ref = do
  node <- readIORef ref
  case node of
    Done value -> do
      return value

    Delayed env prog -> do
      value <- eval env prog
      writeIORef ref (Done value)
      return value

data Undefined = Undefined Name Pos
  deriving stock (Show)
  deriving anyclass (Exception)

data LetCantCapture = LetCantCapture Name Pos
  deriving stock (Show)
  deriving anyclass (Exception)

delay :: Env -> Program -> IO Ref
delay env prog = newIORef $ Delayed env prog

eval :: Env -> Program -> IO Value
eval env prog = case prog of
  Var pos name -> do
    find pos env name >>= force

  Qual pos prog path -> do
    val <- eval env prog
    foldM (get pos) val path

  App pos f x -> do
    fVal <- eval env f
    xRef <- delay env x
    apply pos fVal xRef

  Let pos decls body -> mdo
    env' <- forM decls \case
      Fun pos name decl -> do
        decl' <- delay env'' decl
        return (name, decl')

      Capture pos name -> do
        throwIO $ LetCantCapture name pos

    let env'' = env' <> env
    eval env'' body

  Lam pos args body -> do
    return $ Closure env args body

  Match pos subj alts -> do
    subj' <- delay env subj
    (env', prog') <- select pos subj' alts
    eval (env' <> env) prog'

  Create pos decls -> mdo
    env' <- forM decls \case
      Fun pos name decl -> do
        decl' <- delay env'' decl
        return (name, decl')

      Capture pos name -> do
        decl <- find pos env name
        return (name, decl)

    let env'' = env' <> env
    return $ Object env'

  Lit pos lit -> do
    return $ Constant lit

  Variant pos name arity ->
    return $ Ctor name arity []

get :: Pos -> Value -> Name -> IO Value
get pos val name = case val of
  Object env -> do
    find pos env name >>= force

  _ -> throwIO $ WrongShape (ExpectedObject [name]) pos

data NoMatch = NoMatch Pos
  deriving stock (Show)
  deriving anyclass (Exception)

select :: Pos -> Ref -> [(Pattern, Program)] -> IO (Env, Program)
select pos subj alts = case alts of
  [] -> throwIO (NoMatch pos)
  (pat, branch) : rest -> do
      env <- match subj pat
      return (env, branch)
    `catch` \NoMatch {} -> do
      select pos subj rest

find :: Pos -> Env -> Name -> IO Ref
find pos env name =
  case lookup name env of
    Just ref -> return ref
    Nothing  -> throwIO $ Undefined name pos

data WrongShape = WrongShape Expected Pos
  deriving stock (Show)
  deriving anyclass (Exception)

data Expected
  = ExpectedObject [Name]
  | ExpectedCtorToHaveArity Int Int
  | ExpectedCtor
  | ExpectedLiteral LitKind
  deriving stock (Show)

match :: Ref -> Pattern -> IO Env
match subj pat = do
  case pat of
    IsVar pos name -> do
      return [(name, subj)]

    IsObj pos frame -> do
      val <- force subj
      case val of
        Object env -> do
          foldForM frame $ \(name, mpat) -> do
            subj' <- find pos env name
            case mpat of
              Nothing -> return [(name, subj')]
              Just it -> match subj' it

        _ -> do
          throwIO $ WrongShape (ExpectedObject (map fst frame)) pos

    IsCtor pos name pats -> do
      val <- force subj
      case val of
        Ctor name' 0 refs -> do
          if name == name'
          then do
            when (length pats /= length refs) do
              throwIO $ WrongShape
                (ExpectedCtorToHaveArity (length pats) (length refs))
                pos

            foldForM (zip refs pats) (uncurry match)
          else do
            throwIO $ NoMatch pos

        _ -> do
          throwIO $ WrongShape ExpectedCtor pos

    IsLit pos lit -> do
      val <- force subj
      case val of
        Constant lit' -> do
          if lit == lit'
          then return []
          else
            if litKind lit /= litKind lit'
            then do
              throwIO $ WrongShape (ExpectedLiteral (litKind lit)) pos
            else do
              throwIO $ NoMatch pos

        _ -> do
          throwIO $ WrongShape (ExpectedLiteral (litKind lit)) pos

    Strict pos pat' -> do
      _ <- force subj
      match subj pat'

    Any pos -> do
      return []

foldForM :: (Monoid x, Foldable f, Applicative m) => f a -> (a -> m x) -> m x
foldForM box action = foldr (liftA2 (<>) . action) (pure mempty) box

data TooMuchArguments = TooMuchArguments Pos
  deriving stock (Show)
  deriving anyclass (Exception)

data NotAFunction = NotAFunction Pos
  deriving stock (Show)
  deriving anyclass (Exception)

apply :: Pos -> Value -> Ref -> IO Value
apply pos f x = do
  case f of
    Ctor name arity ys
      | arity > 0 -> do
        return $ Ctor name (arity - 1) (ys ++ [x])

      | otherwise -> do
        throwIO $ NotAFunction pos

    Closure env arg body -> do
      dEnv <- match x arg
      eval (dEnv <> env) body

    Builtin func -> do
      func x

    _ -> throwIO $ NotAFunction pos
