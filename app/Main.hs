
import System.Environment

import ParsingBase (runParser)
import Parsing (Error (Tree))
import Parser
import Lexer
import Program
import Pos
import Eval
import Reconstruct

main = do
  [file] <- getArgs
  txt    <- readFile file

  case runParser lexer (Pos 1 1, txt) of
    (Left errs, (p, s)) -> do
      putStrLn "LEXER:"
      print (show errs)
      putStrLn $ "at " ++ show p

    (Right lexemes, _) -> do
      -- putStrLn
      --   $ reconstruct
      --   $ generateBracesFromIndent
      --   $ lexemes
      case
        runParser program
        $ generateBracesFromIndent
        $ removeComments lexemes
       of
        (Left errs, (p, s) : _) -> do
          putStrLn "PARSER:"
          putStrLn $ "expected" ++ show (Tree "" errs)
          putStrLn $ "at " ++ show p

        (Left errs, _) -> do
          putStrLn "PARSER:"
          putStrLn $ "expected" ++ show (Tree "" errs)
          putStrLn $ "at the end of file"

        (Right prog, _) -> do
          val <- eval [] prog
          print val
