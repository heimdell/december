{ variant Push  x xs
  variant Empty

  fold acc f list =
    let
      loop list = case list of
        | Push x xs -> f x (loop xs)
        | Empty     -> acc
    loop list

  map f list =
    fold
      Empty
      (\x xs -> Push (f x) xs)
      list

  filter pred list =
    let
      insert x xs = case pred x of
        | True  -> Push x xs
        | False -> xs
    fold Empty insert list

  append list other = fold other Push list

  obj = {
    map
    filter
  }

  o = { map; filter }

  l = [
    1
    2
    2
    3
  ]

  l1 = [1,2,3]

  fst (x, _) = x
  snd (_, y) = y

  test =
    let
      variant A
      variant B
      variant C

      variant True
      variant False

      isC x = case x of
        | C -> True
        | _ -> False

      rotate x = case x of
        | A -> B
        | B -> C
        | C -> A

      list = Push A (Push C (Push A Empty))

    filter isC (map rotate list)
}